# Contributing

This site is built using a combination of tools (Gitlab, Middleman, Amazon Web Services, and DatoCMS).

* Gitlab: the site's core code lives here. All the styling, content skeleton (page sections, navigation menu, etc), and scripts for deploying the site are in this codebase.
* [Middleman](https://middlemanapp.com/): This is a generated static website. At build time, content is fetched from DatoCMS, injected into this project's code, and then deployed to Amazon S3.
* [Amazon Web Services](https://aws.amazon.com/console): The static files are hosted on Amazon S3, placed behind a CDN (content delivery network) Amazon Cloudfront. The domain name is registered through Amazon Route53, and the SSL cert is generated through Amazon Certificate Manager.
* [DatoCMS](https://datocms.com): DatoCMS is a content management system like wordpress, but instead of running on a web server (which comes with costs, security vulnerabilities), it is used to generate static websites that can be hosted anywhere.

# Editing content via DatoCMS

## Updating and saving content in DatoCMS

1. Log in to the admin console at https://gnom-ads-com.admin.datocms.com/
2. Choose a content area to edit
3. Make a change and click `Save` in the upper right hand corner

## Publishing saved content

1. Click the `Build status` dropdown in the upper right hand corner
2. Choose either `staging` or `production` and click `Build now`
  - To test changes before publishing to the production site, build the `staging` site first.
  - Changes can be previewed at https://staging.gnom-ads.com
  - Once you're happy with the changes, return to DatoCMS and `Build now` for production
3. To view deploy status, check in DatoCMS at https://gnom-ads-com.admin.datocms.com/admin/deployment_logs or Gitlab at https://gitlab.com/swingbystudios/gnom-ads.com/-/pipelines

Deploys typically take less than 5 minutes.

Video examples of this edit/publish process can viewed in [docs](docs).
