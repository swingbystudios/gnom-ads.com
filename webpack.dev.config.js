// webpack.config.js
const webpack = require('webpack');
const path = require('path');

module.exports = {
  entry: {
    all: path.join(__dirname, '/source/javascripts/main.js')
  },

  output: {
    path: __dirname + '/build',
    filename: 'javascripts/[name].js',
  },

  resolve: {
    modules: [
      path.join(__dirname, 'source'),
      'node_modules'
    ]
  },

  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          { loader: 'style-loader', options: { sourceMap: true } },
          { loader: 'css-loader', options: { sourceMap: true } },
          { loader: 'postcss-loader', options: { sourceMap: true } },
          { loader: 'sass-loader', options: { sourceMap: true } }
        ]
      },
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: ['babel-loader']
      }
    ]
  }
};