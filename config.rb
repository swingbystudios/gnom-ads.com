# Activate and configure extensions
# https://middlemanapp.com/advanced/configuration/#configuring-extensions

require 'slim'
require 'redcarpet'

set :relative_links, true
set :frontmatter_extensions, %w(.html .slim)

set :markdown_engine, :redcarpet

configure :production do
  activate :dato
end

configure :development do
  activate :dato, live_reload: true
  activate :livereload
end

activate :directory_indexes

activate :autoprefixer do |prefix|
  prefix.browsers = "last 2 versions"
end

activate :external_pipeline,
         name: :webpack,
         command: build? ? "npm run build:assets" : "npm run start:assets",
         source: "build",
         latency: 1

# Layouts
# https://middlemanapp.com/basics/layouts/

# Per-page layout changes
page 'index.html', :layout => 'home'

page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

# With alternative layout
# page '/path/to/file.html', layout: 'other_layout'

# Proxy pages
# https://middlemanapp.com/advanced/dynamic-pages/

# proxy(
#   '/this-page-has-no-template.html',
#   '/template-file.html',
#   locals: {
#     which_fake_page: 'Rendering a fake page with a local variable'
#   },
# )

# Helpers
# Methods defined in the helpers block are available in templates
# https://middlemanapp.com/basics/helper-methods/

helpers do
  def nav_active(path)
    current_page.path == path ? 'active' : ''
  end

  def markdown(source)
    Tilt::RedcarpetTemplate.new {source}.render
  end
end

# Build-specific configuration
# https://middlemanapp.com/advanced/configuration/#environment-specific-settings

# configure :build do
#   activate :minify_css
#   activate :minify_javascript
# end
