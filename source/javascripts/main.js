import Glide, { Autoplay, Controls } from '@glidejs/glide/dist/glide.modular.esm'

window.addEventListener('load', function load(event) {

  const slider = new Glide('.glide', {
    autoplay: 6000,
    type: 'carousel'
  });

  slider.mount({ Autoplay, Controls });


  const hamburger = document.querySelector('.nav-button')
  const nav = document.querySelector('nav')

  hamburger.addEventListener('click', function() {
    hamburger.classList.toggle('open')
    nav.classList.toggle('open')
    document.body.classList.toggle('modal-open');
  })

  const navLinks = document.querySelectorAll('nav a');
  for (let i = 0; i < navLinks.length; i++) {
    navLinks[i].addEventListener('click', function() {
      if (nav.classList.contains('open')) {
        nav.classList.remove('open');
        document.body.classList.remove('modal-open');
        hamburger.classList.remove('open')
      }
    })
  }
})